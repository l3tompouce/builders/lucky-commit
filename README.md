# lucky-commit

A `x86_64` build for [not-an-aardvark/lucky-commit](https://github.com/not-an-aardvark/lucky-commit).

> With this simple tool, you can change the start of your git commit
> hashes to whatever you want.

This build uses the `--no-default-features` flag to disable OpenCL
and falls back to a multithreaded CPU implementation.

It also compiles using MUSL in order to get a 100% statically compiled binary.

## Download

The binary is kept as an artifact for an easy download.

* [Binary](https://gitlab.com/l3tompouce/builders/lucky-commit/-/jobs/artifacts/main/raw/lucky_commit?job=build)
* [Version information](https://gitlab.com/l3tompouce/builders/lucky-commit/-/jobs/artifacts/main/raw/lucky_commit.version?job=build)

## git hook

The mandatory `post-commit` hook:

```shell
#!/bin/sh
>&2 echo "Running lucky_commit..."
/usr/local/bin/lucky_commit
```
